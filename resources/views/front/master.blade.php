<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{ my_asset('/uploads/'.env("APP_ICON")) }}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ my_asset('/admin/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ my_asset('/admin/dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <style>
        body:not(.sidebar-mini-md) .content-wrapper,
        body:not(.sidebar-mini-md) .main-header,
        body:not(.sidebar-mini-md) .main-footer
        {
            margin-left: 0px!important;
        }
        .navbar-brand > img {
            max-height: 33px!important;
        }
    </style>
    @yield('css')
</head>
<body class="hold-transition layout-top-nav" ondragstart="return false;" ondrop="return false;" >
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand-md navbar-dark bg-dark">
        <div class="container">

            <a href="{{ url('/') }}" class="navbar-brand">
                <img src="{{ my_asset('/uploads/'.env('APP_LOGO')) }}" alt="Logo" class="image-responsive"
                     style="opacity: .8">
                <span class="brand-text font-weight-light">{{ env("APP_NAME") }}</span>
            </a>

            <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse order-3" id="navbarCollapse">
                <!-- Left navbar links -->
                <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
                    <li class="nav-item">
                        <a href="#" class="nav-link">My Wallet</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/') }}" class="nav-link">Play Now</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">Online Players</a>
                    </li>
                </ul>
            </div>

        </div>
    </nav>
    <!-- /.navbar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container">
                <div class="row mb-2">
                    <div class="col-sm-6"></div>
                    <!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Chess</a></li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    @yield('content')
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        @include('front.online_players')
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
           Play Safe
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; {{ date('Y') }} All rights reserved.
    </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ my_asset('admin/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ my_asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ my_asset('admin/dist/js/adminlte.min.js') }}"></script>
@yield('js')
</body>
</html>
