@extends('front.master')
@section('title', 'Welcome')

@section("css")
    <link rel="stylesheet" href="{{ my_asset('chess/css/reset.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ my_asset('chess/css/main.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ my_asset('chess/css/ios_fullscreen.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ my_asset('chess/css/orientation_utils.css') }}" type="text/css">
    <style>
        #block_gamec {
            position: relative;
            background-color: transparent;
            width: 100%;
            height: 100%;
            display: none;
        }
        .ani_hack0p {
            height: auto!important;
            width: 100%!important;
            top: auto!important;
            left: auto!important;
            position: relative!important;
        }
</style>
@stop

@section('content')
    <div class="col-lg-12 col-sm-12 col-md-12 col-12">

        <div class="check-fonts">
            <p class="check-font-1">arialrounded</p>
        </div>

        <canvas id="canvas" class='ani_hack'></canvas>
        <div data-orientation="portrait" class="orientation-msg-container"><p class="orientation-msg-text">Please rotate your device</p></div>
        <div id="block_game"></div>

    </div>
@stop

@section("js")
    <script type="text/javascript" src="{{ my_asset('chess/js/createjs.min.js') }}"></script>
    <script type="text/javascript" src="{{ my_asset('chess/js/howler.min.js') }}"></script>
    <script type="text/javascript" src="{{ my_asset('chess/js/main.js') }}"></script>
    <script>
        $(document).ready(function(){
            let oMain = new CMain({

                show_score: true,
                start_score: 5000,
                score_decrease_per_second: 10,

                check_orientation: true,
                fullscreen: true
            });


            $(oMain).on("start_session", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeStartSession();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("end_session", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeEndSession();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            $(oMain).on("save_score", function(evt,iWinner, iBlackTime, iWhiteTime, s_iGameType, iWhiteScore) {
                // iWinner:
                // -1 IS DRAW
                //0 WHITE WINS
                //1 BLACK WINS

                if(getParamValue('ctl-arcade') === "true" && s_iGameType === 1 && iWinner === 0){
                    parent.__ctlArcadeSaveScore({score:iWhiteScore, szMode: s_iGameType+""});
                }
            });

            $(oMain).on("share_event", function(evt, iScore, s_iGameType, iWinner) {
                if(getParamValue('ctl-arcade') === "true" && s_iGameType === 1 && iWinner === 0){
                    parent.__ctlArcadeShareEvent({   img: TEXT_SHARE_IMAGE,
                        title: TEXT_SHARE_TITLE,
                        msg: TEXT_SHARE_MSG1 + iScore + TEXT_SHARE_MSG2,
                        msg_share: TEXT_SHARE_SHARE1 + iScore + TEXT_SHARE_SHARE1});
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });


            $(oMain).on("show_interlevel_ad", function(evt) {
                if(getParamValue('ctl-arcade') === "true"){
                    parent.__ctlArcadeShowInterlevelAD();
                }
                //...ADD YOUR CODE HERE EVENTUALLY
            });

            if(isIOS()){
                setTimeout(function(){sizeHandler();},200);
            }else{
                sizeHandler();
            }
        });

    </script>

@stop