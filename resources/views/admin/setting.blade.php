@extends('admin.master')
@section('title', 'Setting')
@section('content-header','Company setting')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-default">
                        <div class="card-header">
                            Update settings
                        </div>
                        <form role="form" autocomplete="off" action="{{ route('setting.company.update') }}" method="post" enctype="multipart/form-data">
                            <div class="card-body row">
                                <div class="col-7">
                                {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="companyName">Company Name</label>
                                        <input type="text" class="form-control" value="{{ env('APP_NAME') }}" name="companyName" id="companyName" placeholder="Company Name"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="companyEmail">Email</label>
                                        <input type="text" class="form-control" value="{{ env('APP_EMAIL') }}" name="companyEmail" id="companyEmail" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="companyLogo">Logo</label>
                                        <input type="file" name="companyLogo" class="form-control" id="companyLogo">
                                    </div>
                                    <div class="form-group">
                                        <label for="companyLogo">Favicon</label>
                                        <input type="file" name="companyIcon" id="companyIcon" class="form-control">
                                    </div>
                                </div>
                                <div class="col-4 text-center">
                                    <div class="col-12">
                                        <br/>
                                        <br/>
                                        <h5>Logo</h5>
                                        <img src="{{ my_asset('uploads/'.env("APP_LOGO")) }}" alt="Logo" style="max-height: 100px" class="img-responsive"/>
                                    </div>
                                    <div class="col-12">
                                        <br/>
                                        <br/>
                                        <h5>Favicon</h5>
                                        <img src="{{ my_asset('uploads/'.env("APP_ICON")) }}" alt="Logo" style="max-height: 100px" class="img-responsive"/>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@stop
