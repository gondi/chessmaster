<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Front'], function() {
    Route::get('/','HomeController@index')->name('home');
});

Route::group(['prefix' => 'admin','namespace' => 'Admin'], function() {

    Route::get('login','LoginController@index')->name('login');
    Route::post('login','LoginController@login')->name('login');
    Route::get('logout','LoginController@logout')->name('logout');

    Route::group(['middleware' => ['auth','auth.admin']], function() {
        Route::get('/', 'DashboardController@index')->name('home');
        Route::get('setting/company', 'SettingController@company')->name('setting.company');
        Route::post('setting/company/update', 'SettingController@companyUpdate')->name('setting.company.update');
        Route::post('user/profile/update', 'UsersController@updateProfile')->name('user.profile.update');
    });

});

