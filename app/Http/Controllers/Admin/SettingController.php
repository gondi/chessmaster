<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DotEnvWriter\DotEnvWriter;

class SettingController extends Controller
{
    public function company(){
        return view("admin.setting");
    }

    public function companyUpdate(Request $request){

        $request->validate([
            'companyName' => 'required',
            'companyEmail' => 'required'
        ]);
        // try {

            $env = new DotEnvWriter('../.env');
            
            $env->set('APP_NAME', $request->companyName);
            $env->set('APP_EMAIL', $request->companyEmail);

            if (!empty($request->companyLogo)) {

                // $request->validate([
                //     'companyLogo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                // ]);

                $companyLogo = 'logo' . time() . '.' . $request->companyLogo->extension();
                $request->companyLogo->move(base_path('assets/uploads'), $companyLogo);
                $env->set('APP_LOGO', $companyLogo);
            }

            if (!empty($request->companyIcon)) {

                // $request->validate([
                //     'companyIcon' => 'required|icons|image|mimes:jpeg,png,jpg,gif,ico,svg|max:2048'
                // ]);

                $companyIcon = 'favicon' . time() . '.' . $request->companyIcon->extension();
                $request->companyIcon->move(base_path('assets/uploads'), $companyIcon);
                $env->set('APP_ICON', $companyIcon);
            }

            $env->save();

            alert()->success("Company updated!");

        //}catch (\Exception $e){
            //alert()->error("Unable to update company, unable to write .env file");
        //}

        return back();

    }
}
