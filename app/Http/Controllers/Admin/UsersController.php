<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function updateProfile(Request $request){

        $user = User::find(Auth::user()->id);

        $request->validate([
            'name' => 'required',
            'email' => 'required'
        ]);

        if (!empty($request->avatar)){
            $request->validate([
                'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048d'
            ]);
            $avatar = 'avatar'.time().'.'.$request->avatar->extension();
            $request->avatar->move(base_path('assets/uploads'), $avatar);

            $user->avatar = $avatar;
        }

        $user->email = $request->email;
        $user->name = $request->name;

        if (!empty($request->password)){
            $user->password = Hash::make($request->password);
        }

        $user->save();

        alert()->success("Profile updated!");

        return back();
    }

}
