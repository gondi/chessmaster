<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        return view('admin.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $remember_me = $request->has('remember') ? true : false;

        $userData = array(
            'email' => $request->email,
            'password' => $request->password
        );

        if (Auth::attempt($userData,$remember_me)) {
            return redirect('/admin');
        } else {
            alert()->error('Check your credentials!','Login failed');
            return redirect('/admin/login');
        }

    }

    public function logout()
    {
        Auth::logout();
        alert()->success("Logged Out!");
        return redirect('/admin/login');
    }
}
