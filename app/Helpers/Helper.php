<?php
function my_asset($path, $secure = null){
    return asset('/assets/' . trim($path, '/'), $secure);
}